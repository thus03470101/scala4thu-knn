/**
  * Created by mark on 23/04/2017.
  */
object KNN extends App{
  val bufferedSource =io.Source.fromFile("knn-dataset.csv")
  val rows: List[TrainRow] ={
    val lines=bufferedSource.getLines()
    ???
  }

  val trainSet=TrainSet(rows)

  println(trainSet.classify(1,List(177,60)))




}

case class TrainRow(label:String,feature:List[Double]){
  def distance(other:List[Double]):Double= ???
}
case class TrainSet(rows:List[TrainRow]){

  def classify(k:Int,input:List[Double]) :String= ???
}